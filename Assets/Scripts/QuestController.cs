﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestController : MonoBehaviour
{
    // GUI
    public RectTransform playerHpBar;
    public RectTransform expBar;
    public RectTransform enemyHpBar;
    public CatSprite sprite;
    public CatSprite enemyCatSprite;

    public StatsPanel playerPanel;
    public StatsPanel enemyPanel;
    public GameObject dataPanel;

    public Text winText;

    // Enemy
    private Enemy enemy;
    private bool inBattle = false;
    private bool enemyIsCat = false;
    private Cat enemyCat;

    /// <summary>
    /// Transfered from GameController
    /// </summary>
    public void StartQuestPanel()
    {
        // Start with no text
        RemoveWinText();
        // Start with no enemy
        enemyPanel.gameObject.SetActive(false);
        // Setup Player Sprite
        sprite.SetSprite(GameController.Instance.GetSelectedCat());
        // Refresh Player panel
        RefreshCatPanel();

    }

    /// <summary>
    /// Start a quest from scratch
    /// </summary>
    public void StartQuest()
    {

    }

    public void RefreshCatPanel()
    {
        // Set Exp bar
        SetExpBar(GameController.Instance.GetSelectedCat().GetExpPercent());
        // Set Health bar
        SetHealthBar(GameController.Instance.GetSelectedCat().GetHpPercent(), true);
        // Set Cat Attributes
        if (GameController.Instance.GetSelectedCat() != null)
        {
            playerPanel.SetAttributes(GameController.Instance.GetSelectedCat().GetCatAttr());
            playerPanel.SetPoints(GameController.Instance.GetSelectedCat().GetPoints());
        }
    }

    public void RefreshEnemyPanel()
    {
        if (enemyIsCat)
        {
            enemyCatSprite.SetSprite(enemyCat);
            enemyCatSprite.gameObject.SetActive(true);
        }
        else
        {
            enemyCatSprite.gameObject.SetActive(false);
        }
        // Set Enemy health
        SetHealthBar((float)enemy.GetRemainingHP() / enemy.GetHp(), false);
        // Set Enemy Attributes
        enemyPanel.SetAttributes(enemy);
        // Turn Panel On
        enemyPanel.gameObject.SetActive(true);
    }

    private void SetHealthBar(float percent, bool player)
    {
        if (player)
        {
            playerHpBar.localScale = new Vector3(percent, 1, 1);
            playerPanel.SetHp(GameController.Instance.GetSelectedCat().GetAttributes());
        }
        else
        {
            enemyHpBar.localScale = new Vector3(percent, 1, 1);
            enemyPanel.SetHp(enemy.GetAttributes());
        }

    }

    private void SetExpBar(float percent)
    {
        expBar.localScale = new Vector3(percent, 1, 1);
    }

    public void NewBattle()
    {
        RemoveWinText();

        if (enemy == null)
        {
            //Generate Enemy
            if(Random.Range(0,100) < 10)
            {
                //Cat
                enemyIsCat = true;
                enemyCat = GameController.Instance.GenerateCat();
                enemy = GameController.Instance.GenerateEnemyCat(enemyCat);

            }
            else
            {
                //Prey or Monster
                enemyIsCat = false;
                enemy = GameController.Instance.GenerateEnemy(GameController.Instance.GetSelectedCat().data.level);
            }


            //TODO turn on enemy panel
            RefreshEnemyPanel();
        }

        //Restore Cat
        GameController.Instance.GetSelectedCat().RestoreHP();

        inBattle = true;
    }

    public void PlayerTurn()
    {
        if (inBattle)
        {
            int cAtk = GameController.Instance.GetSelectedCat().GetDamage();
            int eHP = enemy.TakeDamage(cAtk, GameController.Instance.GetSelectedCat().GetStl());
            //AppendText("Player cat attacks for " + cAtk + ".");
            SetHealthBar((float)eHP / enemy.GetHp(), false);
            //AppendText("Enemy hp: " + eHP + "/" + enemy.GetHp());
            if (eHP <= 0)
            {
                GameController.Instance.GetSelectedCat().ApplyExp(enemy.level);
                Debug.Log("Enemy is Cat: " + enemyIsCat);
                if (enemyIsCat)
                {
                    GameController.Instance.AddCat(enemyCat);
                    SetWinText("Win: New Cat!");
                }
                else
                {
                    SetWinText();
                }
                

                if (Random.Range(0, 1) < 1)
                {
                    // TODO
                    //AppendText("Picked up: " + DropItem().name);
                }

                Debug.Log(GameController.Instance.GetSelectedCat().CatToString());

                EndBattle();
            }
            else
            {
                EnemyTurn();
            }
        }

    }


    private void EndBattle()
    {
        enemyPanel.gameObject.SetActive(false);
        inBattle = false;
        enemy = null;
        GameController.Instance.GetSelectedCat().RestoreHP();
        RefreshCatPanel();
    }

    private void EnemyTurn()
    {
        int eAtk = enemy.GetDamage();
        bool defeated = GameController.Instance.GetSelectedCat().TakeDamage(eAtk, enemy.GetStl());
        SetHealthBar(GameController.Instance.GetSelectedCat().GetHpPercent(), true);

        if (defeated)
        {
            SetLossText();
            EndBattle();
        }
    }

    public void NavUp()
    {
        Nav("Up");
    }

    public void NavDown()
    {
        Nav("Down");
    }

    public void NavLeft()
    {
        Nav("Left");
    }

    public void NavRight()
    {
        Nav("Right");
    }

    /// <summary>
    /// 50% Chance of starting a battle.
    /// </summary>
    /// <param name="dir"></param>
    public void Nav(string dir)
    {
        if (inBattle == false)
        {
            if (Random.Range(0, 2) > 0)
            {
                NewBattle();
            }
            else
            {
                //TODO Nothing?
            }
        }
    }

    /// <summary>
    /// End a battle
    /// </summary>
    public void Retreat()
    {
        //TODO show text on screen
        if (inBattle)
        {
            EndBattle();
        }
    }

    private void SetLossText()
    {
        winText.text = "Loss";
        winText.color = Color.red;
        winText.gameObject.SetActive(true);
    }
    private void SetWinText()
    {
        winText.text = "Win";
        winText.color = Color.green;
        winText.gameObject.SetActive(true);
    }
    private void SetWinText(string s)
    {
        winText.text = s;
        winText.color = Color.green;
        winText.gameObject.SetActive(true);
    }

    private void RemoveWinText()
    {
        winText.gameObject.SetActive(false);
    }
}
