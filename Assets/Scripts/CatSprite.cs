﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatSprite : MonoBehaviour
{
    public Image baseColor;
    public Image lines;
    public Image eyesEars;
    //public Image[] markings;

    //public void SetSprite(Dictionary<string,Sprite> dict, CatData data)
    //{
    //    ClearSprite();
    //    baseColor.color = data.color;
    //    eyesEars.color = data.eyeColor;
    //    //for(Marking in data.marking)
    //    SetMarking(dict[data.marking], data.markingColor);

    //    eyesEars.transform.SetAsLastSibling();
    //    lines.transform.SetAsLastSibling();
    //}

    /// <summary>
    /// Setup the cat sprite with markings and colors
    /// </summary>
    /// <param name="cat"></param>
    public void SetSprite(Cat cat)
    {
        ClearSprite();
        baseColor.color = cat.data.color;
        eyesEars.color = cat.data.eyeColor;
        SetMarking(cat.GetMarking(), cat.data.markingColor);

        eyesEars.transform.SetAsLastSibling();
        lines.transform.SetAsLastSibling();
    }

    private void SetMarking(Sprite s, Color c)
    {
        GameObject obj = new GameObject();
        Image img = obj.AddComponent<Image>();
        img.sprite = s;
        // Change color
        img.color = c;
        // Change size
        RectTransform rt = obj.GetComponent<RectTransform>();
        RectTransform prt = baseColor.gameObject.GetComponent<RectTransform>(); //Copying position from the base layer

        rt.offsetMin = new Vector2(0,0);
        rt.offsetMax = new Vector2(0, 0);
        rt.anchorMin = prt.anchorMin;
        rt.anchorMax = prt.anchorMax;
        rt.pivot = prt.pivot;

        // Parent Panel
        obj.transform.SetParent(transform, false);
    }

    private void ClearSprite()
    {
        Debug.Log("transform: " + transform);
        foreach (Transform child in transform)
        {
            if(child.name !="BaseColor" & child.name != "EarEyeColor" & child.name != "Lines")
            GameObject.Destroy(child.gameObject);
        }
    }
}


[Serializable]
public class Marking{
    public string marking;
    public Sprite sprite;
}




