﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsPanel : MonoBehaviour
{
    public Text hp;
    public Text str;
    public Text acc;
    public Text stl;

    public Text nameStr;
    public Text lv;
    public Text points;

    public void SetAttributes(RPGAttributes attr)
    {
        nameStr.text = attr.name;
        lv.text = "Lv "+attr.level+"";
        hp.text = attr.remainingHp +" / "+ attr.hp;
        str.text = attr.strength + "";
        acc.text = attr.accuracy + "";
        stl.text = attr.stealth + "";
    }

    public void SetPoints(int points)
    {
        this.points.text = points+"";
    }

    public void SetHp(RPGAttributes attr)
    {
        hp.text = attr.remainingHp + " / " + attr.hp;
    }


}
