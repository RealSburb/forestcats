﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat
{
    public CatData data;
    private int expPerLv = 100;
    //private Dictionary<string, Sprite> markingSprites;
    private Sprite marking;

    public void SetupCat(CatData data, Dictionary<string, Sprite> dict)
    {
        this.data = data;
        marking = dict[data.marking];
        //markingSprites = new Dictionary<string, Sprite>();
        //markingSprites.Add(data.marking, dict[data.marking]);
    }

    public string CatToString()
    {
        char nl = '\n';
        string s = "";
        s += "Name: " + data.name + nl;
        s += "Pursonality: " + data.personality + nl;
        s += "Lv: " + data.level + nl;
        s += "Exp: " + data.exp + "/" + expPerLv + nl;
        s += "Points: " + data.points + nl;
        s += "Hp: " + data.hp + nl;
        s += "Strength: " + data.strength + nl;
        s += "Accuracy: " + data.accuracy + nl;
        s += "Stealth: " + data.stealth + nl;
        s += "Color: " + data.color + nl;
        s += "Marking: " + data.marking + nl;
        return s;
    }



    /// <summary>
    /// Apply experience points and check for levelup
    /// </summary>
    /// <param name="exp"></param>
    /// <returns></returns>
    public void ApplyExp(int lvBeaten)
    {
        int lvDiff = lvBeaten - data.level;
        int expApplied = 0;
        // If level 0 then automatically level up on a singular win
        if (data.level == 0)
        {
            LevelUp();
        }

        // Apply earned experience towards the next level
        if (lvDiff < 0)
        {
            expApplied = 10 - data.level;
            if(expApplied < 0)
            {
                expApplied = 0;
            }
        }
        else if (lvDiff > 0)
        {
            expApplied = 10 + 5 * lvDiff;
        }
        else // same lv
        {
            expApplied = 10;
        }


        int expSum = data.exp + expApplied;

        if (expSum < expPerLv)
        {
            data.exp += expApplied;
        }
        else
        {
            // calc leftover
            int leftover = expSum - expPerLv;
            // apply
            LevelUp();
            data.exp = leftover;
        }
    }

    /// <summary>
    /// increase level, stats and reset exp
    /// </summary>
    private void LevelUp()
    {
        data.level++;
        data.points++;
        //RandomStatUp();
    }

    /// <summary>
    /// TODO Remove -- Placeholder until stat assignment GUI is done
    /// </summary>
    private void RandomStatUp()
    {
        int stat = UnityEngine.Random.Range(0, 4); 
        Debug.Log("statup " + (Stat)stat);
        switch ((Stat)stat)
        {
            case Stat.hp:
                data.hp += 2;
                //Debug.Log("HPup " + data.hp);
                break;
            case Stat.accuracy:
                data.accuracy++;
                //Debug.Log("ACCup " + data.accuracy);
                break;
            case Stat.stealth:
                data.stealth++;
                //Debug.Log("STLup " + data.stealth);
                break;
            case Stat.strength:
                data.strength++;
                //Debug.Log("STRup " + data.strength);
                break;
            default:
                Debug.Log(stat + " not a valid Stat");
                break;
        }
    }

    /// <summary>
    /// Restore Hp by an amount
    /// </summary>
    /// <param name="amt"></param>
    public void RestoreHP(int amt)
    {
        int maxHp = data.hp;
        if (data.remainingHp + amt <= maxHp)
        {
            data.remainingHp += amt;
        }
        else // remaining hp + amt > hpmax
        {
            data.remainingHp = maxHp;
        }
    }

    /// <summary>
    /// Restore Hp to Max
    /// </summary>
    public void RestoreHP()
    {
        RestoreHP(data.hp);
        //Debug.Log("MaxHP:" + data.hp);
        //Debug.Log("RemainingHp:" + remainingHp);
    }

    /// <summary>
    /// Get a random damage value between the floor and ceiling of the cat's damage window
    /// </summary>
    /// <returns></returns>
    public int GetDamage()
    {
        int damage = Random.Range(data.accuracy, data.strength + 1);
        return damage;
    }

    /// <summary>
    /// Subtract damage taken and return true for player defeat, or false for continuing
    /// </summary>
    /// <param name="dmg"></param>
    /// <returns></returns>
    public bool TakeDamage(int dmg, int stl)
    {
        int fdmg = dmg - (int)((data.stealth - stl) * 0.5);

        if (data.remainingHp - fdmg > 0) // If this attack will not ko the player
        {
            if (fdmg > 0) // if dmg can go down
            {
                data.remainingHp -= fdmg;
            }
            return false;
        }
        else //
        {
            data.remainingHp = 0;
            return true;
        }

    }

    /// <summary>
    /// Increase a specific stat by one point
    /// </summary>
    /// <param name="s"></param>
    public void IncreaseStat(Stat s)
    {
        if (data.points > 0)
        {

            switch (s)
            {
                case Stat.hp:
                    data.hp += 2;
                    break;
                case Stat.accuracy:
                    if(data.accuracy+1 <= data.strength) // Accuracy cannot go higher than strength
                    {
                        data.accuracy++;
                    }
                    else
                    {
                        return;
                    }
                    break;
                case Stat.stealth:
                    data.stealth++;
                    break;
                case Stat.strength:
                    data.strength++;
                    break;
                default:
                    Debug.Log(s + " not a valid Stat");
                    break;
            }
            data.points--;
        }
    }
    public RPGAttributes GetAttributes()
    {
        return data;
    }



    // UTILITY
    //public int GetMaxHP() { return data.maxHp; }
    public float GetHpPercent() { return (float)data.remainingHp / data.hp; }
    public float GetExpPercent() { return (float)data.exp / expPerLv; }
    public string GetRemainingHPString() { return (data.remainingHp + "/" + data.hp); }
    public RPGAttributes GetCatAttr() { return (RPGAttributes)data; }
    public int GetStl() { return data.stealth;}
    public int GetPoints() { return data.points; }
    public Color GetColor() { return data.color; }
    public Sprite GetMarking() { return marking; }
}

[System.Serializable]
public class CatData : RPGAttributes
{
    // -- Local Id --
    public int localId;
    // -- Personality --
    public string personality;

    // -- Attribute Values --
    public int exp = 0;
    public int points = 0; // Amt points left to spend

    // -- Visual Attributes --
    public Color color;
    public Color eyeColor;
    public Color markingColor;
    public string marking;
}

/// <summary>
/// Used to set stat points on cats.
/// </summary>
public enum Stat
{
    hp,
    strength,
    accuracy,
    stealth
}

/// <summary>
/// Used to assign values to both the enemy and cat stat panels
/// </summary>
[System.Serializable]
public abstract class RPGAttributes
{
    public string name;
    public int level = 0;
    public int remainingHp = 6;
    public int hp = 6;
    public int strength = 1;
    public int accuracy = 0;
    public int stealth = 0;
}