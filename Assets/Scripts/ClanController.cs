﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClanController : MonoBehaviour
{
    public Transform clanScrollContent;
    Dictionary<Cat, CatProfile> profiles = new Dictionary<Cat, CatProfile>();

    public void RefreshView()
    {
        // Remove all children
        foreach (Transform child in clanScrollContent)
        {
            GameObject.Destroy(child.gameObject);
            profiles.Clear();
        }
        // Add all Cats
        foreach (Cat c in GameController.Instance.GetListOfCats())
        {
            CatProfile cp = Instantiate(GameController.Instance.profilePrefab,clanScrollContent).GetComponent<CatProfile>();
            cp.SetupProfile(c);
            profiles.Add(c, cp); // add profile to array
        }
        // Turn on highlight
        Cat selCat = GameController.Instance.GetSelectedCat();
        if(selCat != null)
        {
            SelectProfile(selCat);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        RefreshView();
    }

    public void SelectProfile(Cat c)
    {
        profiles[c].selectionIndicator.color = Color.yellow;
    }

    public void DeselectProfile(Cat c)
    {
        profiles[c].selectionIndicator.color = Color.white;
    }
}
