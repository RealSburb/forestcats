﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[System.Serializable]
public class Item
{
    public string name;
}


public class GameController : GenericSingletonClass<GameController>
{
    // SubControllers
    public ClanController cc;
    // Dictionaries
    public Item[] itemDefinitions;
    public EnemyType[] enemies;
    public Marking[] markings;
    private Dictionary<string, Sprite> markingDict = new Dictionary<string, Sprite>();

    // Player Dictionaries
    private Dictionary<int, Cat> cats = new Dictionary<int, Cat>();
    Dictionary<Item, int> inventory = new Dictionary<Item, int>(); // ItemObj #ofItem

    // Utilities
    private int nextId = 0;
    private CatMaker catMaker;

    // Prefabs
    public Transform profilePrefab;

    // Player
    private Cat selectedCat;

    public Dictionary<string, Sprite> GetMarkingDict()
    {
        return markingDict;
    }


    /// <summary>
    /// Add item to inventory. Increase # if already exists, otherwise add it to the inventory
    /// </summary>
    /// <param name="i"></param>
    public void AddItem(Item i)
    {
        if (inventory.ContainsKey(i))
        {
            inventory[i]++;
        }
        else
        {
            inventory.Add(i, 1);
        }
    }

    /// <summary>
    /// Return string representation of the inventory
    /// </summary>
    /// <returns></returns>
    public string InventoryToString()
    {
        string str = "Inventory:\n";
        foreach (Item i in inventory.Keys)
        {
            str += inventory[i] + " " + i.name + '\n';
        }
        return str;
    }


    // Start is called before the first frame update
    void Start()
    {
        // Setup Dictionaries
        foreach(Marking m in markings)
        {
            markingDict.Add(m.marking,m.sprite);
        }
        
        // Setup CatMaker
        catMaker = this.GetComponent<CatMaker>();

        // Setup Cat
        GenerateStarterCat();
        GenerateStarterCat();
        Debug.Log("Default Selected Cat: " + selectedCat);
    }

    public Cat GetSelectedCat()
    {
        return selectedCat;
    }

    /// <summary>
    ///  Select the clicked cat and deselect the old cat
    /// </summary>
    /// <param name="c"></param>
    public void SetSelectedCat(Cat c)
    {
        cc.DeselectProfile(selectedCat);
        cc.SelectProfile(c);
        selectedCat = c;
    }

    /// <summary>
    /// Generate Random enemy at a lv
    /// </summary>
    /// <param name="i"></param>
    public Enemy GenerateEnemy(int lv)
    {
        return new Enemy(enemies[UnityEngine.Random.Range(0, enemies.Length)], PickTier(lv));
    }

    /// <summary>
    /// Generate Random enemy cat
    /// </summary>
    /// <param name="i"></param>
    public Enemy GenerateEnemyCat(Cat c)
    {
        return new Enemy(c, PickTier(selectedCat.data.level));
    }

    /// <summary>
    /// Create a Cat Object with stats
    /// </summary>
    /// <returns></returns>
    public Cat GenerateCat()
    {
        Cat c = new Cat();
        c.SetupCat(catMaker.GenerateCat(nextId++), markingDict);
        return c;
    }

    private EnemyTier PickTier(int lv)
    {
        if (lv <= 5)
        {
            return EnemyTier.Beginner;
        }
        else if (lv <= 10)
        {
            return EnemyTier.Novice;
        }
        else if (lv <= 20)
        {
            return EnemyTier.Intermediate;
        }
        else if (lv <= 30)
        {
            return EnemyTier.Advanced;
        }
        else if (lv <= 50)
        {
            return EnemyTier.Experienced;
        }
        else if (lv <= 80)
        {
            return EnemyTier.Hard;
        }
        else
        {
            return EnemyTier.Expert;
        }
    }

    /// <summary>
    /// Create a generic cat, add it to the owned cats, and set it selected
    /// </summary>
    public void GenerateStarterCat()
    {
        selectedCat = GenerateCat();
        cats.Add(nextId++,selectedCat);
        Debug.Log("starter selected cat: " + selectedCat);
    }

    /// <summary>
    /// When a cat battle is won add the cat to the clan
    /// </summary>
    /// <param name="c"></param>
    public void AddCat(Cat c)
    {
        cats.Add(nextId++,c);
        Debug.Log("Added New Cat: " + c);
    }
 

    /// <summary>
    /// Drop one item
    /// </summary>
    public Item DropItem()
    {
        return DropItems(1)[0];
    }

    /// <summary>
    /// Add a number of items to player inventory
    /// </summary>
    /// <param name="num"></param>
    public Item[] DropItems(int num)
    {
        Item[] drops = new Item[num];
        for (int i = 0; i < num; i++)
        {
            int itmNum = UnityEngine.Random.Range(0, itemDefinitions.Length);
            Item itm = itemDefinitions[itmNum];
            AddItem(itm);
            drops[i] = itm;
        }
        return drops;
    }

    /// <summary>
    /// Increase a single stat by 1
    /// </summary>
    /// <param name="stat"></param>
    public void IncreaseCatStat(int stat)
    {
        selectedCat.IncreaseStat((Stat)stat);
    }

    /// <summary>
    /// Used to switch between views
    /// </summary>
    /// <param name="o"></param>
    public void ToggleView(GameObject o)
    {
        o.SetActive(!o.activeSelf);
    }

    /// <summary>
    /// Return a list of Cat Objects
    /// </summary>
    /// <returns></returns>
    public List<Cat> GetListOfCats()
    {
        return cats.Values.ToList<Cat>();
    }
}