﻿using UnityEngine;
using UnityEngine.UI;

public class CatProfile : MonoBehaviour
{
    private Cat cat;
    private CatSprite sprite;
    public Text lvText;
    public Text nameText;
    public Text genderText;
    public Image selectionIndicator;

    public void SetupProfile(Cat cat)
    {
        this.cat = cat;
    }

    public void Clicked()
    {
        GameController.Instance.SetSelectedCat(cat);
    }

    // Start is called before the first frame update
    void Start()
    {
        if(cat != null)
        {
            sprite = GetComponentInChildren<CatSprite>();
            sprite.SetSprite(cat);
            lvText.text = "Lv" + cat.data.level;
            nameText.text = cat.data.name;
            genderText.text = "?";
        }
    }
}
